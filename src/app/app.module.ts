import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { EmployersComponent } from './employers/employers.component';
import { RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdTabsModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatListModule} from '@angular/material';
import { FormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    EmployersComponent,
  ],
  imports: [
    MatButtonModule,
    FormsModule,
    MatListModule,
    MatInputModule,
    MdTabsModule,
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule.forRoot([
    {
      path: 'home',
      component: HomeComponent
    },
    {
      path: 'about',
      component: AboutComponent
    },
    {
      path: 'employers',
      component: EmployersComponent
    }
  ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
